$(document).ready(function() {



  console.log('color', $('.bfh-colorpicker input').val());

  // Add minus icon for collapse element which is open by default
  $(".collapse.in").each(function() {
    $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
  });

  // Toggle plus minus icon on show hide of collapse element
  $(".collapse").on('show.bs.collapse', function() {
    $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    //Отображаем блок с превью выбраной настройки
    $('.displaying-section-item').css({
      'display': 'none',
      'opacity': 0
    });
    $('.displaying-section').css({
      'height': '600px'
    });
    var elemDisplaing = '.' + $(this).parent().attr('data-setting-elem');
    $(elemDisplaing).css({
      'display': 'block',
      'opacity': 1
    });
  }).on('hide.bs.collapse', function() {
    $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    //Скрываем блок с превью выбраной настройки
    var toCloseElem = '.' + $(this).parent().attr('data-setting-elem');
    $(toCloseElem).css({
      'display': 'none',
      'opacity': 1
    });
    $('.displaying-section').css({
      'height': '0px'
    });
  });
});



//Настройка форм
var firstForm = {
  "bgColor": "#ffffff",
  "titleColor": "#ffffff",
  "titleFontSize": "20",
  "btnColor": "#ff8300",
  "btnFontColor": "#ffffff",
  "btnFontSize": "10"
};
var secondForm = {
  "bgColor": "#575757",
  "titleColor": "#ffffff",
  "titleFontSize": "20",
  "btnColor": "#ff8300",
  "btnFontColor": "#ffffff",
  "btnFontSize": "22"
};
var thirdForm = {
  "bgColor": "#ff6d4f",
  "titleColor": "#ffffff",
  "titleFontSize": "20",
  "btnColor": "#7ed23d",
  "btnFontColor": "#335e12",
  "btnFontSize": "18"
};
var forthForm = {
  "bgColor": "#7a729e",
  "titleColor": "#ffffff",
  "titleFontSize": "20",
  "btnColor": "#65beb7",
  "btnFontColor": "#ffffff",
  "btnFontSize": "22"
};

//Настройка кнопки перзвонить
var firstRecallButton = {
  "bgColor": "#268efc"

};
var secondRecallButton = {
  "bgColor": "#268efc"

};
var thirdRecallButton = {
  "bgColor": "#837ba6"

};
var forthRecallButton = {
  "bgColor": "#f6731c"

};

//total users setting
var firstTotalUsers = {
  "bgColor": "#ffffff",
  "textColor": "#000000",
  "fontSize": "15",
  "borderColor": "#ffffff"
};
var secondTotalUsers = {
  "bgColor": "#ffffff",
  "textColor": "#000000",
  "fontSize": "15",
  "borderColor": "#ffffff"
};
var thirdTotalUsers = {
  "bgColor": "#ff3466",
  "textColor": "#ffffff",
  "borderColor": "#ff3466"
};
var forthTotalUsers = {
  "bgColor": "#ffffff",
  "textColor": "#000000",
  "fontSize": "15",
  "borderColor": "#ffffff"
};


//online users setting
var firstOnlineUsers = {
  "bgColor": "#eb524a",
  "textColor": "#ffffff",
  "fontSize": "14",
  "borderColor": "#ffffff"
};
var secondOnlineUsers = {
  "bgColor": "#ffffff",
  "textColor": "#666871",
  "fontSize": "18",
  "borderColor": "#ffffff"
};
var thirdOnlineUsers = {
  "bgColor": "#ff3466",
  "textColor": "#ffffff",
  "borderColor": "#ff3466"
};
var forthOnlineUsers = {
  "bgColor": "#ffffff",
  "textColor": "#000000",
  "fontSize": "14",
  "borderColor": "#ffffff"
};

//Настройка формы и изменение настроек
function form_setting_change(settingArr) {

  $(".recall-form-bg-color input").val(settingArr.bgColor);
  $(".recall-form-bg-color input").attr("placeholder", settingArr.bgColor);
  $('.recall-form-bg-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.bgColor
  });

  $(".recall-form-title-color input").val(settingArr.titleColor);
  $(".recall-form-title-color input").attr("placeholder", settingArr.titleColor);
  $('.recall-form-title-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.titleColor
  });

  $(".recall-form-title-font-size input").val(settingArr.titleFontSize);


  $(".recall-form-btn-color input").val(settingArr.btnColor);
  $(".recall-form-btn-color input").attr("placeholder", settingArr.btnColor);
  $('.recall-form-btn-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.btnColor
  });
  $(".recall-form-btn-title-color input").val(settingArr.btnFontColor);
  $(".recall-form-btn-title-color input").attr("placeholder", settingArr.btnFontColor);
  $('.recall-form-btn-title-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.btnFontColor
  });

  $(".recall-form-btn-font-size input").val(settingArr.btnFontSize);

  $('.form-design-selector').css({
    'background-color': settingArr.bgColor
  });
  $('.form-title-selector').css({
    'color': settingArr.titleColor,
    'font-size': settingArr.titleFontSize
  });
  $('.form-design-selector button').css({
    'background-color': settingArr.btnColor,
    'color': settingArr.btnFontColor,
    'font-size': settingArr.btnFontSize
  });
  $('.clbh_banner-after').css({
    'border-left-color': settingArr.bgColor
  });

}

function total_user_change_setting(settingArr) {
  //text color setting
  $('.total-users-text-color input').val(settingArr.textColor);

  $('.total-users-text-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.textColor
  });


  //font setting
  $(".total-user-font option:selected").text('Helvetica');

  //font size
  $(".total-user-font-size input").val(settingArr.fontSize);


  //border color setting
  $('.total-users-border-color input').val(settingArr.borderColor);
  $('.total-users-border-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.borderColor
  });

  //bg color setting
  $('.total-users-bg-color input').val(settingArr.bgColor);
  $('.total-users-bg-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.bgColor
  });



}

function online_user_change_setting(settingArr) {

  console.log('online');
  //text color setting
  $('.online-users-text-color input').val(settingArr.textColor);
  $('.online-users-text-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.textColor
  });
  //font setting
  $(".online-user-font option:selected").text('Helvetica');

  //font size
  $(".online-user-font-size input").val(settingArr.fontSize);


  //border color setting
  $('.online-users-border-color input').val(settingArr.borderColor);
  $('.online-users-border-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.borderColor
  });
  //bg color setting
  $('.online-users-bg-color input').val(settingArr.bgColor);
  $('.online-users-bg-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.bgColor
  });

}

function recall_button_change_setting(settingArr) {
  $('.recall-button-color input').val(settingArr.bgColor);
  $('.recall-button-color .bfh-colorpicker-icon').css({
    'background-color': settingArr.bgColor
  });
}

$(function() {

  default_setting_elems();

  $('.radio').click(function() {

    //defaultSetting in form
    //total-user defaults
    if ($('#optionsTotalUsers1').is(':checked')) {
      total_user_change_setting(firstTotalUsers);

    } else if ($('#optionsTotalUsers2').is(':checked')) {
      total_user_change_setting(secondTotalUsers);

    } else if ($('#optionsTotalUsers3').is(':checked')) {
      total_user_change_setting(thirdTotalUsers);

    } else if ($('#optionsTotalUsers4').is(':checked')) {
      total_user_change_setting(forthTotalUsers);
    }

    //user online defaultSetting

    if ($('#optionsOnlineUsers1').is(':checked')) {
      online_user_change_setting(firstOnlineUsers);

    } else if ($('#optionsOnlineUsers2').is(':checked')) {

      online_user_change_setting(secondOnlineUsers);
    } else if ($('#optionsOnlineUsers3').is(':checked')) {
      online_user_change_setting(thirdOnlineUsers);

    } else if ($('#optionsOnlineUsers4').is(':checked')) {
      online_user_change_setting(forthOnlineUsers);

    }


    //recall button defaultSetting
    if ($('#optionsRecallButton1').is(':checked')) {
      recall_button_change_setting(firstRecallButton);
    } else if ($('#optionsRecallButton2').is(':checked')) {
      recall_button_change_setting(secondRecallButton);
    } else if ($('#optionsRecallButton3').is(':checked')) {
      recall_button_change_setting(thirdRecallButton);
    } else if ($('#optionsRecallButton4').is(':checked')) {
      recall_button_change_setting(forthRecallButton);
    }

    //displaing total user item
    if ($(this).attr('id') == 'optionsTotalUsers1') {
      $('.total-user-wrapper').css({
        'display': 'none'
      });
      $('.first-design').css({
        'display': 'flex'
      });
    } else if ($(this).attr('id') == 'optionsTotalUsers2') {
      $('.total-user-wrapper').css({
        'display': 'none'
      });
      $('.second-design').css({
        'display': 'flex'
      });


    } else if ($(this).attr('id') == 'optionsTotalUsers3') {
      $('.total-user-wrapper').css({
        'display': 'none'
      });
      $('.third-design').css({
        'display': 'block'
      });

    } else if ($(this).attr('id') == 'optionsTotalUsers4') {
      $('.total-user-wrapper').css({
        'display': 'none'
      });
      $('.forth-design').css({
        'display': 'flex'
      });

    }
    //displaing online user item
    else if ($(this).attr('id') == 'optionsOnlineUsers1') {
      $('.now-on-site-wrapper').css({
        'display': 'none'
      });
      $('.now-on-site-wrapper.first').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsOnlineUsers2') {
      $('.now-on-site-wrapper').css({
        'display': 'none'
      });
      $('.now-on-site-wrapper.second').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsOnlineUsers3') {
      $('.now-on-site-wrapper').css({
        'display': 'none'
      });
      $('.now-on-site-wrapper.third').css({
        'display': 'block',
        'opacity': 1
      });

    } else if ($(this).attr('id') == 'optionsOnlineUsers4') {
      $('.now-on-site-wrapper').css({
        'display': 'none'
      });
      $('.now-on-site-wrapper.forth').css({
        'display': 'flex',
        'opacity': 1
      });

    }
    //displaing order item
    else if ($(this).attr('id') == 'optionsOrderNow1') {
      $('.order-now-selector').css({
        'display': 'none'
      });
      $('.first-design-setting.order-now-selector').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsOrderNow2') {
      $('.order-now-selector').css({
        'display': 'none'
      });
      $('.second-design-setting.order-now-selector').css({
        'display': 'block',
        'opacity': 1
      });


    }
    //displaing freeze item
    else if ($(this).attr('id') == 'optionsFreeze1') {


      $('.freeze-selector').css({
        'display': 'none'
      });
      $('.freeze-example3').css({
        'display': 'block',
        'opacity': 1
      });




    } else if ($(this).attr('id') == 'optionsFreeze2') {
      $('.freeze-selector').css({
        'display': 'none'
      });
      $('.freeze-example1').css({
        'display': 'block',
        'opacity': 1
      });

    } else if ($(this).attr('id') == 'optionsFreeze3') {

      $('.freeze-selector').css({
        'display': 'none'
      });
      $('.freeze-example2').css({
        'display': 'block',
        'opacity': 1
      });


    }
    //displaing recall buttons
    else if ($(this).attr('id') == 'optionsRecallButton1') {
      $('.call-button').css({
        'display': 'none'
      });
      $('.first-call-button').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsRecallButton2') {
      $('.call-button').css({
        'display': 'none'
      });
      $('.second-call-button').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsRecallButton3') {
      $('.call-button').css({
        'display': 'none'
      });
      $('.third-call-button').css({
        'display': 'flex',
        'opacity': 1
      });


    } else if ($(this).attr('id') == 'optionsRecallButton4') {
      $('.call-button').css({
        'display': 'none'
      });
      $('.forth-call-button').css({
        'display': 'flex',
        'opacity': 1
      });


    }
    //displaing recall forms
    else if ($(this).attr('id') == 'optionsRecallForm1') {
      $('.form-design-selector').css({
        'display': 'none'
      });
      form_setting_change(firstForm);
      $('.first-design-form').css({
        'display': 'block',
        'opacity': 1
      });
      if ($(window).width() > 768) {
        $('.displaying-section').css({
          'overflow-x': 'inherit'
        });
      }


    } else if ($(this).attr('id') == 'optionsRecallForm2') {
      $('.form-design-selector').css({
        'display': 'none'
      });
      form_setting_change(secondForm);

      $('.second-design-form').css({
        'display': 'block',
        'opacity': 1
      });
      if ($(window).width() > 768) {
        $('.displaying-section').css({
          'overflow-x': 'inherit'
        });
      }


    } else if ($(this).attr('id') == 'optionsRecallForm3') {
      $('.form-design-selector').css({
        'display': 'none'
      });
      form_setting_change(thirdForm);

      $('.third-design-form').css({
        'display': 'block',
        'opacity': 1
      });
      if ($(window).width() > 768) {
        $('.displaying-section').css({
          'overflow-x': 'inherit'
        });
      }


    } else if ($(this).attr('id') == 'optionsRecallForm4') {
      $('.form-design-selector').css({
        'display': 'none'
      });
      $('.displaying-section').css({
        'overflow-x': 'scroll'
      });
      form_setting_change(forthForm);
      console.log('forth');

      $('.forth-design-form').css({
        'display': 'block',
        'opacity': 1
      });


    }
  });
});
$(function() {
  //positon enabled
  $('.top-positon .top-position-input').on('change', function() {
    console.log('val', $(this));
    if ($(this).val() != "") {
      $(this).parent().parent().find('.bottom-position-input').attr("disabled", true);

    } else {
      $(this).parent().parent().find('.bottom-position-input').attr("disabled", false);

    }
  });
  $('.bottom-positon .bottom-position-input').on('change', function() {
    if ($(this).val() != "") {
      $(this).parent().parent().find('.top-position-input').attr("disabled", true);
    } else {
      $(this).parent().parent().find('.top-position-input').attr("disabled", false);

    }
  });

  $('.left-positon .left-position-input').on('change', function() {
    console.log('val', $(this));
    if ($(this).val() != "") {
      $(this).parent().parent().find('.right-position-input').attr("disabled", true);

    } else {
      $(this).parent().parent().find('.right-position-input').attr("disabled", false);

    }
  });
  $('.right-positon .right-position-input').on('change', function() {
    if ($(this).val() != "") {
      $(this).parent().parent().find('.left-position-input').attr("disabled", true);
    } else {
      $(this).parent().parent().find('.left-position-input').attr("disabled", false);

    }
  });
  //script display timer
  $('.bfh-number-btn').click(function() {
    var timer = $(this).parent().find('.bfh-number').val();
    if ($(this).hasClass('inc')) {
      timer++;
      $(this).parent().find('.bfh-number').val(timer);
    } else if ($(this).hasClass('dec')) {
      if (timer > 0) {
        timer--;
      }
      $(this).parent().find('.bfh-number').val(timer);
    }
  });

  //font family changer
  $('.font-select').on('change', function() {
    if ($(this).hasClass('total-user-font')) {
      $('.total-user-displaying .total-user-wrapper').css('font-family', this.value);
    } else if ($(this).hasClass('online-user-font')) {
      $('.online-user-displaying .now-on-site-wrapper').css('font-family', this.value);
    } else if ($(this).hasClass('recall-button-font')) {
      $('.recall-displaying .call-button').css('font-family', this.value);
    } else if ($(this).hasClass('recall-form-font')) {
      $('.recall-displaying .form-design-selector').css('font-family', this.value);
    } else if ($(this).hasClass('order-now-font')) {
      $('.order-now-selector').css('font-family', this.value);
    }
  });


  //font size changer
  $('.font-size-select').on('input', function() {
    if ($(this).hasClass('total-user-font-size')) {
      console.log($(this).find('input').val());
      $('.total-user-displaying .total-user-wrapper').css('font-size', $(this).find('input').val() + 'px');
    } else if ($(this).hasClass('online-user-font-size')) {
      $('.online-user-displaying .now-on-site-wrapper').css('font-size', $(this).find('input').val() + 'px');
    } else if ($(this).hasClass('recall-button-font-size')) {
      $('.recall-displaying .call-button').css('font-size', $(this).find('input').val() + 'px');
    } else if ($(this).hasClass('recall-form-title-font-size')) {
      $('.form-title-selector').css('font-size', $(this).find('input').val() + 'px');
      if ($('#optionsRecallForm1').is(':checked')) {
        firstForm.titleFontSize = $(this).find('input').val();

      } else if ($('#optionsRecallForm2').is(':checked')) {
        secondForm.titleFontSize = $(this).find('input').val();

      } else {
        thirdForm.titleFontSize = $(this).find('input').val();

      }
    } else if ($(this).hasClass('recall-form-btn-font-size')) {
      $('.form-design-selector button').css('font-size', $(this).find('input').val() + 'px');

      if ($('#optionsRecallForm1').is(':checked')) {
        firstForm.btnFontSize = $(this).find('input').val();

      } else if ($('#optionsRecallForm2').is(':checked')) {
        secondForm.btnFontSize = $(this).find('input').val();

      } else {
        thirdForm.btnFontSize = $(this).find('input').val();

      }
    } else if ($(this).hasClass('recall-pop-up-title-font-size')) {
      $('.bottom-recall-title').css({
        'font-size': $(this).find('input').val() + 'px'
      });
    }
  });
  //Script switcher
  $('.script-switcher').on('change', function() {
    var scriptBlockName = $(this).val();
    $(this).parent().parent().parent().find('.input').first('input').prop('checked', true);

  });


});

function hexc(colorval) {
  var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  delete(parts[0]);
  for (var i = 1; i <= 3; ++i) {
    parts[i] = parseInt(parts[i]).toString(16);
    if (parts[i].length == 1) parts[i] = '0' + parts[i];
  }
  color = '#' + parts.join('');
  return color;
}

function default_setting_elems() {

  //text color setting
  $('.total-users-text-color input').val(hexc($('.total-user-displaying .first-design').css('color')));
  console.log('def');
  console.log($('.total-users-text-color .bfh-colorpicker-icon'));
  console.log(hexc($('.total-user-displaying .first-design').css('color')));

  $('.total-users-text-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.total-user-displaying .first-design').css('color'))
  });
  //font setting
  $(".total-user-font option:selected").text('Helvetica');
  //font size
  $(".total-user-font-size input").val($('.total-user-displaying .first-design').css('fontSize').substr(0, $('.total-user-displaying .first-design').css('fontSize').length - 2));
  //border color setting
  $('.total-users-border-color input').val(hexc($('.total-user-displaying .first-design').css('borderColor')));
  $('.total-users-border-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.total-user-displaying .first-design').css('borderColor'))
  });

  //bg color setting
  $('.total-users-bg-color input').val(hexc($('.total-user-displaying .first-design').css('backgroundColor')));
  $('.total-users-bg-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.total-user-displaying .first-design').css('backgroundColor'))
  });

  //text color setting
  $('.online-users-text-color input').val(hexc($('.online-user-displaying .first').css('color')));
  $('.online-users-text-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.online-user-displaying .first').css('color'))
  });

  //font setting
  $(".online-user-font option:selected").text('Helvetica');
  //font size
  $(".online-user-font-size input").val($('.online-user-displaying .first').css('fontSize').substr(0, $('.online-user-displaying .first').css('fontSize').length - 2));
  //border color setting
  $('.online-users-border-color input').val(hexc($('.online-user-displaying .first').css('borderColor')));
  $('.online-users-border-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.online-user-displaying .first').css('borderColor'))
  });

  //bg color setting
  $('.online-users-bg-color input').val(hexc($('.online-user-displaying .first').css('backgroundColor')));
  $('.online-users-bg-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.online-user-displaying .first').css('backgroundColor'))
  });

  $('.recall-button-color input').val(hexc($('.first-call-button .img-circle').css('backgroundColor')));
  $('.recall-button-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.first-call-button .img-circle').css('backgroundColor'))
  });

  //bottom recall pop up default setting
  $('.recall-pop-up-title-color input').val(hexc($('.bottom-recall-title').css('color')));
  $('.recall-pop-up-title-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.bottom-recall-title').css('color'))
  });

  //font size
  $(".recall-pop-up-title-font-size input").val($('.bottom-recall-title').css('fontSize').substr(0, $('.bottom-recall-title').css('fontSize').length - 2));
  //bg color setting
  $('.recall-pop-up-form-bg-color input').val(hexc($('.bottom-recall-form').css('backgroundColor')));
  $('.recall-pop-up-form-bg-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.bottom-recall-form').css('backgroundColor'))
  });

  //button-color
  $('.recall-pop-up-form-btn-color input').val(hexc($('.bottom-recall-button').css('backgroundColor')));
  $('.recall-pop-up-form-btn-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.bottom-recall-button').css('backgroundColor'))
  });

  //button text color
  $('.recall-pop-up-form-btn-title-color input').val(hexc($('.bottom-recall-button').css('color')));
  $('.recall-pop-up-form-btn-title-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.bottom-recall-button').css('color'))
  });

  //font setting
  $(".recall-pop-up-form-font option:selected").text('Helvetica');

  //pseudochat setting
  // block bg
  $('.pseudo-chat-bg-color input').val(hexc($('.pseudo-chat-displaying').css('backgroundColor')));
  $('.pseudo-chat-bg-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.pseudo-chat-displaying').css('backgroundColor'))
  });
  //title color
  $('.pseudo-chat-title-color input').val(hexc($('#chat').css('color')));
  $('.pseudo-chat-title-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('#chat').css('color'))
  });
  //header panel
  $('.pseudo-chat-header-color input').val(hexc($('.chat-label').css('backgroundColor')));
  $('.pseudo-chat-header-color .bfh-colorpicker-icon').css({
    'background-color': hexc($('.chat-label').css('backgroundColor'))
  });
}