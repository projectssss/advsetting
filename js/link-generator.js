$(function() {
  $('#generate-link-btn').click(function() {

    var domen = 'http://xgeka2jo.beget.tech/index.php';
    var link = domen + '?including-scripts=';
    $.each($('.script-switcher'), function(index, value) {
      if ($(value).is(':checked')) {
        var scriptName = $(value).data("script-name");
        link += scriptName + '-';
      }
    });
    link += '&';
    if ($('#landing-multilanguages').is(':checked')) {
      link = link + 'landing-multilanguages=1' + '&';
    }
    if ($('#total-users-switcher').is(':checked')) {
      //design variant
      if ($('#optionsTotalUsers1').is(':checked')) {
        link = link + 'total-users[design-var]=1' + '&';
      } else if ($('#optionsTotalUsers2').is(':checked')) {
        link = link + 'total-users[design-var]=2' + '&';
      } else if ($('#optionsTotalUsers3').is(':checked')) {
        link = link + 'total-users[design-var]=3' + '&';
      } else if ($('#optionsTotalUsers4').is(':checked')) {
        link = link + 'total-users[design-var]=4' + '&';
      }
      //text color setting
      link = link + 'total-users[text-color]=' + $('.total-users-text-color input').val().slice(1) + '&';

      //font setting
      link = link + 'total-users[font]=' + $(".total-user-font option:selected").text() + '&';

      //font size
      link = link + 'total-users[font-size]=' + $(".total-user-font-size input").val() + '&';

      //border color setting
      link = link + 'total-users[border-color]=' + $('.total-users-border-color input').val().slice(1) + '&';

      //bg color setting
      link = link + 'total-users[bg-color]=' + $('.total-users-bg-color input').val().slice(1) + '&';

      //display timer
      link = link + 'total-users[display-timer]=' + ($('.total-user-disp-timer input').val() * 1000) + '&';

    }
    if ($('#online-users-switcher').is(':checked')) {
      //design variant
      if ($('#optionsTotalUsers1').is(':checked')) {
        link = link + 'online-users[design-var]=1' + '&';
      } else if ($('#optionsTotalUsers2').is(':checked')) {
        link = link + 'online-users[design-var]=2' + '&';
      } else if ($('#optionsTotalUsers3').is(':checked')) {
        link = link + 'online-users[design-var]=3' + '&';
      } else if ($('#optionsTotalUsers4').is(':checked')) {
        link = link + 'online-users[design-var]=4' + '&';
      }
      //text color setting
      link = link + 'online-users[text-color]=' + $('.online-users-text-color input').val().slice(1) + '&';

      //font setting
      link = link + 'online-users[font]=' + $(".online-user-font option:selected").text() + '&';

      //font size
      link = link + 'online-users[font-size]=' + $(".online-user-font-size input").val() + '&';

      //bg color setting
      link = link + 'online-users[bg-color]=' + $('.online-users-bg-color input').val().slice(1) + '&';

      //border color setting
      link = link + 'online-users[border-color]=' + $('.online-users-border-color input').val().slice(1) + '&';

      //display timer
      link = link + 'online-users[display-timer]=' + ($('.online-user-disp-timer input').val() * 1000) + '&';

    }

    if ($('#order-now-switcher').is(':checked')) {
      //design variant
      if ($('#optionsOrderNow1').is(':checked')) {
        link = link + 'landing-orders[design-var]=1' + '&';
      } else if ($('#optionsOrderNow2').is(':checked')) {
        link = link + 'landing-orders[design-var]=2' + '&';
      }
      //geo setting
      if ($('#geo-setting').is(':checked')) {
        link = link + 'landing_geo=1' + '&';

      }

      //font setting
      link = link + 'landing-orders[font]=' + $(".order-now-font option:selected").text() + '&';

      //display timer
      link = link + 'landing-orders[display-timer]=' + ($('.order-now-disp-timer input').val() * 1000) + '&';

    }

    if ($('#freeze-switcher').is(':checked')) {
      //design variant
      if ($('#optionsFreeze1').is(':checked')) {
        link = link + 'landing-freeze[design-var]=1' + '&';
      } else if ($('#optionsFreeze2').is(':checked')) {
        link = link + 'landing-freeze[design-var]=2' + '&';
      } else if ($('#optionsFreeze3').is(':checked')) {
        link = link + 'landing-freeze[design-var]=3' + '&';
      }

      //display timer
      link = link + 'landing-freeze[display-timer]=' + ($('.freeze-disp-timer input').val() * 1000) + '&';

    }

    if ($('#recall-switcher').is(':checked')) {
      //design variant button
      if ($('#optionsRecallButton1').is(':checked')) {
        link = link + 'landing-recall-btn[design-var]=1' + '&';
      } else if ($('#optionsRecallButton2').is(':checked')) {
        link = link + 'landing-recall-btn[design-var]=2' + '&';
      } else if ($('#optionsRecallButton3').is(':checked')) {
        link = link + 'landing-recall-btn[design-var]=3' + '&';
      } else if ($('#optionsRecallButton4').is(':checked')) {
        link = link + 'landing-recall-btn[design-var]=4' + '&';
      }
      //design variant form
      if ($('#optionsRecallForm1').is(':checked')) {
        link = link + 'landing-recall-form[design-var]=1' + '&';
      } else if ($('#optionsRecallForm2').is(':checked')) {
        link = link + 'landing-recall-form[design-var]=2' + '&';
      } else if ($('#optionsRecallForm3').is(':checked')) {
        link = link + 'landing-recall-form[design-var]=3' + '&';
      } else if ($('#optionsRecallForm4').is(':checked')) {
        link = link + 'landing-recall-form[design-var]=4' + '&';
      }
      //button setting

      //bg color setting
      link = link + 'landing-recall-btn[bg-color]=' + $('.recall-button-color input').val().slice(1) + '&';

      //display timer
      link = link + 'landing-recall-btn[display-timer]=' + $('.order-now-disp-timer input').val() + '&';

      //form setting

      //text color setting
      link = link + 'landing-recall-form[title-text-color]=' + $('.recall-form-title-color input').val().slice(1) + '&';

      link = link + 'landing-recall-form[title-font-size]=' + $('.recall-form-title-font-size input').val() + '&';

      //font setting
      link = link + 'landing-recall-form[font]=' + $(".recall-form-font option:selected").text() + '&';

      //bg-btn color setting
      link = link + 'landing-recall-form[btn-bg-color]=' + $('.recall-form-btn-color input').val().slice(1) + '&';

      //button color setting
      link = link + 'landing-recall-form[btn-title-color]=' + $('.recall-form-btn-title-color input').val().slice(1) + '&';

      //btn font-size
      link = link + 'landing-recall-form[btn-font-size]=' + $('.recall-form-btn-font-size input').val() + '&';

      //btn bg-color
      link = link + 'landing-recall-form[form-bg-color]=' + $('.recall-form-bg-color input').val().slice(1) + '&';

      //display timer
      link = link + 'landing-recall-form[display-timer]=' + ($(".recall-timer input").val() * 1000) + '&';


    }
    if ($('#recall-pop-up-switcher').is(':checked')) {

      //bg color setting
      link = link + 'landing-bottom-popup-form[bg-color]=' + $('.recall-pop-up-form-bg-color input').val().slice(1) + '&';

      //button color setting
      link = link + 'landing-bottom-popup-form[title-color]=' + $('.recall-form-btn-title-color input').val().slice(1) + '&';

      //btn font-size
      link = link + 'landing-bottom-popup-form[title-font-size]=' + $('.recall-form-btn-font-size input').val() + '&';

      //btn bg-color
      link = link + 'landing-bottom-popup-form[btn-bg-color]=' + $('.recall-form-bg-color input').val().slice(1) + '&';

      //btn font color
      link = link + 'landing-bottom-popup-form[btn-font-color]=' + $('.recall-pop-up-form-btn-title-color input').val().slice(1) + '&';

      //display timer
      link = link + 'landing-bottom-popup-form[display-timer]=' + ($(".recall-timer input").val() * 1000) + '&';

    }
    if ($('#pseudo-chat-switcher').is(':checked')) {

      //bg color setting
      link = link + 'pseudo-chat[bg-color]=' + $('.pseudo-chat-bg-color input').val().slice(1) + '&';

      //header font color setting
      link = link + 'pseudo-chat[title-color]=' + $('.pseudo-chat-title-color input').val().slice(1) + '&';

      //header bg color setting
      link = link + 'pseudo-chat[header-color]=' + $('.pseudo-chat-header-color input').val().slice(1) + '&';

    }
    console.log('link', link);

    $('.generated-link').val(link);
    $('.generated-link').attr("placeholder", link);

  });
});